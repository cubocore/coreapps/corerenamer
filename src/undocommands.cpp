/*
	*
	* This file is a part of CoreRenamer.
	* A batch file renamer for C Suite.
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include <QStandardItem>

#include "undocommands.h"

ARRTextCommand::ARRTextCommand(QStandardItem *item, const QString &addedT, COMMAND c, QUndoCommand *parent)
	: QUndoCommand(parent)
	, m_item(item)
	, m_nText(addedT)
	, m_oText(item->text())
{
	setText(getCommandText(c) + " Changed from \"" + item->text() + "\" to \"" + m_nText + "\"");
}

void ARRTextCommand::undo()
{
	if (m_item) {
		m_item->setText(m_oText);
	}
}

void ARRTextCommand::redo()
{
	if (m_item) {
		m_item->setText(m_nText);
	}
}

QString ARRTextCommand::getCommandText(COMMAND c)
{
	switch (c) {
		case ADD:
			return "ADD";

		case REMOVE:
			return "REMOVE";

		case REPLACE:
			return "REPLACE";

		case NUMBER:
			return "NUMBER";

		case CASE:
			return "CASE";

		default:
			return "Unknown";
	}
}
